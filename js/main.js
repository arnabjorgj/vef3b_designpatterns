/**
 * Created by 2701942819 on 11.2.2016.
 */
// Iife til að gera scope utan um kóðann
(function(window, Handlebars) {
    "use strict";

    // window. til að gera App að global breytu
    window.App = {
        container: null,
        init: function() {
            this.container = $('#container');

            // Placeholder issue?

            // Þegar hash parturinn af url-inu breytist keyrist render fallið
            // .bind(this) => this = App en ekki window.
            window.onhashchange = this.render.bind(this);
            this.render();
        },

        // router function
        render: function() {
            var container = this.container;

            var url = window.location.hash.substr(1);
            if (url.match(/^details\/[0-9]+$/)) {
                var id = url.split('/')[1];
                controller2.render(id, function(html) {
                    container.html(html);
                });
            }
            else {
                controller.render(function(html) {
                    container.html(html);
                }, function (error) {
                    container.notify(error);
                });
            }
        }
    };

    var Api = {
        events: [],

        getJson: function (onSuccess, onError) {
            var self = this;
            $.get('http://apis.is/concerts').done(function(data) {
                self.events = [];
                data.results.forEach(function(eventData) {
                    self.events.push(new EventModel(eventData));
                });
                onSuccess(self.events);
            }).fail(onError);
        },
        getEvent: function(id) {
            return this.events[id];
        }
    };

    // The Model
    var EventModel = function (data) {
        this.title = data.eventDateName;
        this.subTitle = data.name;
        this.venue = data.eventHallName;
        this.location = data.userGroupName;
        this.image = data.imageSource;
        this.date = moment(data.dateOfShow);
        this.relativeDate = this.date.fromNow();
        this.formattedDate = this.date.format('LLL');
    };

    // Controller fyrir view 1
    var controller = {
        render: function(onSuccess, onError) {
            $.get("templates/template1.handlebars").done(function (template) {
                var template = Handlebars.compile(template);
                Api.getJson(function (events) {
                    onSuccess(template(events));
                }, function () {
                    onError('Something went wrong. We are sorry.');
                });
            });
        }
    };

    // Controller fyrir view 2
    var controller2 = {
        render: function(id, onSuccess) {
            var event = Api.getEvent(id);
            $.get("templates/template2.handlebars").done(function(template) {
                var template = Handlebars.compile(template);
                onSuccess(template(event));
            });
        }
    };


})(window,Handlebars);

App.init();

/* Búa til map f. staðsetningu
 function createMap() {
     var mapProp = {
         center:new google.maps.LatLng(64.136186, -21.915807),
         zoom: 7,
         mapTypeId: google.maps.MapTypeId.ROADMAP
     };
     var map = new google.maps.Map(document.getElementById("map"), mapProp);
 } */


/*
    Picture placeholder issue:
    http://stackoverflow.com/questions/27837134/image-error-load-placeholder-image-issue-on-mobile-safari-chrome
 */